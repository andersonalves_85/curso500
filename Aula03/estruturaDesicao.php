<?php
$aluno = "marta";
$bimestre1 = 10;
$bimestre2 = 7;
$bimestre3 = 10;
$bimestre4 = 9;
$faltas = 10;
$aulas = 50;

/**
 * calcular a media do aluno
 *
 * exibir
 * -nome
 * -notas
 * -media
 * -status
 * seguite condiçoes media >=7 e porcentagem de faltas <=25% aprovado
 * media <7 e porcentagem de faltas >25% reprovado
 * media <7 e porcentagem de faltas <=25% recuperação
 * media >= 7 e porcentagem de faltas >25% reprovado
 */

$media = (($bimestre1 + $bimestre2 + $bimestre3 + $bimestre4) / 4);
$porcentagem = ($faltas / $aulas) * 100;
// echo $porcentagem;

/**
 * If($media >= "7" or $porcentagem <= "25" ){
 * $status == "aprovado";}
 * Else {
 * if ($faltas <= "25") {
 * $status == "recuperação"}
 *
 * }
 * $satus == "reprovado";
 *
 *
 * echo "o aluno" . $aluno . "com as notas " . $bimestre1 . " ". $bimestre2. " ". $bimestre3. " ".$bimestre4. " ".
 * "com media " .$media . "obteve o status de ";
 */

if ($porcentagem > 25) {
    $status = "reprovado";
} elseif ($media < 7) {
    $status = "recuperação";
} else {
    $status = "aprovado";
}

echo 'nome: ' . ($aluno ? "$aluno<br>" : 'Aluno sem nome' . '<br>');

echo "o aluno " . $aluno . " com as notas " . $bimestre1 . " , " . $bimestre2 . " , " . $bimestre3 . " , " . $bimestre4 . " , " . " com media " . $media . " obteve o status de  " . $status;