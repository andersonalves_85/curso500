<?php
declare (strict_types =1);

function media(float $n1,float $n2,float $n3,float $n4):float
{
    $media =($n1+$n2+$n3+$n4) / 4;
    $media = (string) $media;
    return $media;
}
$media = media(7, 10, 10, 10);
echo $media;

echo '<br>';

var_dump($media);