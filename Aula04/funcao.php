<?php

function negrito($string)
{
    echo "<strong>$string</strong>";
}

function quebra()
{
    echo '<br>';
}

negrito("trabalho com Funções");
quebra();
negrito("Outra");

function titulo($titulo, $tamanho = 2)
{
    echo "<h$tamanho>$titulo</h$tamanho";
}

titulo("novo titulo");

echo '<hr>';

function media($n1,$n2,$n3,$n4)
{
    return($n1+$n2+$n3+$n4) /4;  
}


echo '<hr>';
echo 'Media: ' . media(10, 5, 7.5, 8);

echo '<hr>';

echo  media(10, 5, 7.5, 8) >= 7.625 ? 'Aprovado' : 'Reprovado';
