<?php require 'verifica_login.php';?>


<html>
<head>
<title>Listagem de Alunos</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>


<body>
<?php include 'menu.php';?>



<div id="main">
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nome</th>
      <th scope="col">Serie</th>
      <th scope="col">Turma</th>
      
    </tr>
  </thead>
  <tbody>
  <?php  
  //inicio do fluxo 
  require 'conexao.php';
    
  $query = 'select * from alunos order by id';
  $result = pg_query($query);
  $alunos = pg_fetch_all($result);
    
  foreach ($alunos as $alunos):
  
  ?>
    <tr>
      <th scope="row"><?=  $alunos['id']?></th>
      <td><?=  $alunos['nome']?></td>
      <td><?=  $alunos['serie']?></td>
      <td><?=  $alunos['turma']?></td>
      <td>
      <a href="alterar_aluno.php?id=<?= $alunos['id'] ?>">Alterar</a> |
      <a href="excluir_aluno.php?id=<?= $alunos['id'] ?>">Excluir</a>
      
      </td>
     
    </tr>
     <?php endforeach;  ?>
  </tbody>
</table>

</div>


</body>

</html>


