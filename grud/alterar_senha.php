<?php require 'verifica_login.php';?>
<html>
	<head>
		<title>Autenticação</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<?php require 'menu.php'; ?>
		
		
        <div id="main" class="container col-sm-5 bg-light" style="margin-top:80px">
        	<div class="row">
        		<h2>Alterar Senha</h2>
        	</div>
        	<div class="row" style="margin-top:30px; padding:5px">
        	<?php 
        	   require 'conexao.php';
        	   //http://dontpad.com/4linux/php/500/alterar_senha
        	   if($_POST){
        	       $senha = $_POST['senha'];
        	       $confirmaSenha = $_POST['confirmaSenha'];
        	       
        	       if(empty($senha)){
        	           $errorSenha = '<div class="row col-sm-10 alert alert-danger">
                                          <h5>A senha é obrigatória</h5>
                                      </div>';
        	       }
        	       
        	       if(empty($confirmaSenha)){
        	           $errorConfirmaSenha = '<div class="row col-sm-10 alert alert-danger">
                                                <h5>Confirme a senha</h5>
                                              </div>';
        	       }
        	       
        	       if($senha != $confirmaSenha){
        	           $errorNaoConfere = '<div class="row col-sm-10 alert alert-danger">
                                                <h5>As senhas não conferem</h5>
                                              </div>';
        	       }
        	       
        	       
        	       
        	       $id = $_SESSION['id'];
        	       
        	       $query = "update usuarios set senha='$senha'
                                where id = $id
                            ";
        	       
        	       if(! isset($errorSenha) && ! isset($errorConfirmaSenha) && 
        	           ! isset($errorNaoConfere)){
        	           $result = pg_query($query);
        	           if($result){
        	               echo '<div class="row col-sm-10 alert alert-success">
                                      <h5>Senha Alterada com Sucesso!</h5>
                                  </div>';
        	           }else{
        	               echo '<div class="row col-sm-10 alert alert-danger">
                                   <h5>Erro ao salvar os dados</h5>
                                 </div>';
        	           }
        	       }
        	   }
        	?>
        	<?= isset($errorNaoConfere) ? $errorNaoConfere : '' ?>
        	<form action="" method="post">
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Senha</label>
                <div class="col-sm-10">
                  <input type="password" name ="senha" value="<?= isset($_POST['senha']) ? $_POST['senha'] : '' ?>" class="form-control" id="inputEmail3" placeholder="Digite a senha">
                  <?= isset($errorSenha) ? $errorSenha : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Confirmar Senha</label>
                <div class="col-sm-10">
                  <input type="password" name="confirmaSenha" value="<?= isset($_POST['confirmaSenha']) ? $_POST['confirmaSenha'] : '' ?>" class="form-control" id="inputPassword3" placeholder="Confirmar Senha">
                  <?= isset($errorConfirmaSenha) ? $errorConfirmaSenha : '' ?>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-10">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>
        </div>
        </div>
     </body>
</html>