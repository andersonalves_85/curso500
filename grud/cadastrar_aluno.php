<?php require 'verifica_login.php';?>

<html>
<head>
<title>Listagem de Alunos</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>


<body>
<?php include 'menu.php';?>


<div id ="main">

<?php 
   require 'conexao.php';
   if($_POST){
       
       if(empty($_POST['nome'])){           
           $errorNome = '<div class="alert alert-danger"role="alert">
  <strong>Atenção!</strong> a senha e obrigatorio.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       
           
       }
       
       if(empty($_POST['serie'])){
           $errorSerie = '<div class="alert alert-danger"role="alert">
  <strong>Atenção</strong> A serie e obrigatorio.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       }
       
       if(empty($_POST['turma'])){
           $errorTurma = '<div class="alert alert-danger"role="alert">
  <strong>Atenção!</strong> A Turma e obrigatória.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       }
       
       $nome = $_POST ['nome'];
       $serie = $_POST ['serie'];
       $turma = $_POST ['turma'];
       $query = "insert into alunos 
               (nome,serie,turma)
            values('$nome','$serie', '$turma')";
       
       $result = false;
       
       if(! isset($errorNome) &&
          ! isset($errorSerie) &&
           ! isset($errorTurma)){
                  $result = pg_exec($query);
         
                  if($result){
                      header('location:listar_aluno.php');
                  }else {
                      echo '<div class="alert alert-danger"role="alert"><h1>Erro ao salvar os dados!</h1> </div>';
                  }
                  
       }
           
              
       
   }

?>

           <form action="" method="post" >
              <div class="form-group row">
                <label for="inputnome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-5">
                  <input type="text" name ="nome" value ="<?= isset($_POST['nome'])? $_POST['nome'] : '' ?>" class="form-control" id="inputNome3" placeholder="Nome">
                <?= isset($errorNome)? $errorNome:''?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputSerie" class="col-sm-2 col-form-label">Serie</label>
                <div class="col-sm-5">
                  <input type="text" name ="serie" value ="<?= isset($_POST['serie'])? $_POST['serie'] : '' ?>" class="form-control" id="inputSerie" placeholder="Serie">
                <?= isset($errorSerie)? $errorSerie:''?>
               </div>
              </div>
              <div class="form-group row">
                <label for="inputTurma" class="col-sm-2 col-form-label">Turma</label>
                <div class="col-sm-5">
                  <input type="text" name ="turma" value ="<?= isset($_POST['turma'])? $_POST['turma'] : '' ?>"  class="form-control" id="inputTurma" placeholder="Turma">
                <?= isset($errorTurma)? $errorTurma:''?>
                </div>
              </div>
              
              <div class="form-group row">
                <div class="col-sm-5">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>

</div>

</body>
</html>



