<?php require 'verifica_login.php';?>

<html>
<head>
<title>Listagem de usuario</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>


<body>
<?php include 'menu.php';?>
<?php  require 'verifica_perfil.php';?>

<div id ="main">

<?php 
   require 'conexao.php';
   if($_POST){
       
       if(empty($_POST['nome'])){           
           $errorNome = '<div class="alert alert-danger"role="alert">
  <strong>Atenção!</strong> a senha e obrigatorio.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       
           
       }
       
       if(empty($_POST['email'])){
           $errorEmail = '<div class="alert alert-danger"role="alert">
  <strong>Atenção</strong> O email e obrigatorio.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       }
       
       if(empty($_POST['senha'])){
           $errorSenha = '<div class="alert alert-danger"role="alert">
  <strong>Atenção!</strong> A senha e obrigatória.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       }
       
       if(empty($_POST['perfil'])){
           $errorPerfil = '<div class="alert alert-danger"role="alert">
  <strong>Atenção!</strong> o Perfil é obrigatório.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
       }
       
       $nome = $_POST ['nome'];
       $email = $_POST ['email'];
       $senha = $_POST ['senha'];
       $perfil = $_POST ['perfil'];
       $query = "insert into usuarios 
               (nome,email,senha,perfil)
            values('$nome','$email', '$senha', '$perfil')";
       
       $result = false;
       
       if(! isset($errorNome) &&
          ! isset($errorEmail) &&
           ! isset($errorSenha) &&
            ! isset($errorPerfil)){
                  $result = pg_exec($query);
         
                  if($result){
                      header('location:listar_usuario.php');
                  }else {
                      echo '<div class="alert alert-danger"role="alert"><h1>Erro ao salvar os dados!</h1> </div>';
                  }
                  
       }
           
              
       
   }

?>

           <form action="" method="post" >
              <div class="form-group row">
                <label for="inputnome" class="col-sm-2 col-form-label">Nome</label>
                <div class="col-sm-5">
                  <input type="text" name ="nome" value ="<?= isset($_POST['nome'])? $_POST['nome'] : '' ?>" class="form-control" id="inputNome3" placeholder="Nome">
                <?= isset($errorNome)? $errorNome:''?>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-5">
                  <input type="email" name ="email" value ="<?= isset($_POST['email'])? $_POST['email'] : '' ?>" class="form-control" id="inputEmail3" placeholder="E-mail">
                <?= isset($errorEmail)? $errorEmail:''?>
               </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword3" class="col-sm-2 col-form-label">Senha</label>
                <div class="col-sm-5">
                  <input type="senha" name ="senha" value ="<?= isset($_POST['senha'])? $_POST['senha'] : '' ?>"  class="form-control" id="inputPassword3" placeholder="Senha">
                <?= isset($errorSenha)? $errorSenha:''?>
                </div>
              </div>
              
              <div class="form-group row">                           
                <label for= "inputPerfil" class="col-sm-2 col-form-label">Perfil</label>
                <div class="col-sm-5">
                  <select name ="perfil" class="custom-select">
                    <option value="" selected>Selecionar o Perfil</option>
                    <option value="1">Administrador</option>
                    <option value="2">Secretario</option>  
                  </select>
                <?= isset($errorPerfil)? $errorPerfil:''?>
                </div>
              </div>
              
              <div class="form-group row">
                <div class="col-sm-5">
                  <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>

</div>

</body>
</html>


