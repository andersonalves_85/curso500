<?php
$array = [
    'Ana APula',
    'Patricia',
    'Isabela Jorge',
    'Carolina Ferraz'
        
];

sort($array);

echo '<pre>';

print_r($array);

echo '<hr>';

$array2 = [
    10=>'Ana APula',
    7=>'Patricia',
    3=>'Isabela Jorge',
    1=>'Carolina Ferraz'
    
];

ksort($array2);
echo '<pre>';
print_r($array2);

echo '<hr>';

var_dump(array_key_exists('nome',['nome' => 'maria']));

echo '<br>';

var_dump( in_array('Maria',['nome' => 'maria']));

echo '<br>';

var_dump( array_search('Maria',['nome' => 'maria']));

echo '<hr>';

$array3 = [
    'a@Gmail.com',
    'b@gmail.com',
    'cGmail.com',
    'd@Gmail.com',
     'a@mail.com',
    'b@Gmail.com',
    'cGmail.com',
    'd@gmail.com'
];

echo '<br>';
print_r(array_unique($array3));


echo '<hr>';

print_r(array_merge($array ,$array2));


$produtosFiltrados = array_filter($array3, function($email) {
    return strstr($email, '@gmail');
});

print_r($produtosFiltrados);


echo '<hr>';
echo '<br>';
echo count($array3);
